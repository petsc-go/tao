TAO
===

A [Go](http://golang.org/) package for calling functions from
[TAO](http://www.mcs.anl.gov/tao/) (Toolkit for Advanced Optimization).
