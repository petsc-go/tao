TAO Examples
============

Examples of calling functions from [TAO](http://www.mcs.anl.gov/tao/)
(Toolkit for Advanced Optimization) using the [Go](http://golang.org/)
programming language.

  - [Rosenbrock](./rosenbrock1/rosenbrock1.go)
